## SpringDuck Mobile Robot
![Alt text](./images/springduck.png "SpringDuck Image")

## SpringDuck Architecture
![Alt text](./images/springduck_architecture.svg "SpringDuck Architecture")

### SpringDuck Sensors
* IMU 9 Axis
* Lidar 16 Channel
* Sonar 5 Channel
* Speaker
* Bumper
* 4Wheel 2 Axis 
* GNSS
* Voltage Sensor
* 3.5' TFTLCD(touchscreen)
* Front Camera
* Support Hotspot

### SpringDuck 기능
* Wired Joystick Control

## Roadmap

### 7월
* make a odometry package base on the ros_controller

### 8월
* LiDAR 기반 측위 기술 적용 

### todo list
* ~~SpringGO Router(only Software Ver.) 적용~~
* ~~Trailer 기반의 서비스 구현~~
* ~~SpringDock #1 1.0(Prototype) Release~~


