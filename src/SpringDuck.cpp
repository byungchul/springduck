
#include <signal.h>
#include <boost/chrono.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/assign/list_of.hpp>
#include <system_error>
#include "SpringDuck.h"
#include "controller_manager/controller_manager.h"
#include "ros/callback_queue.h"
#include "tf/tf.h"

#ifndef PI
  #define PI 3.14159265359
#endif

namespace spring
{
constexpr int encoder_pluse	= 1000*4; // 1000x4체배
constexpr int gear_ratio = 53; // 1/53(모터)
constexpr int i_cpr = encoder_pluse*gear_ratio; // 212000
constexpr double f_wheel_diameter = 0.275; // 0.046 for test
constexpr double f_wheel_separation= 0.51; // lenght between two wheels from center
constexpr double distance_per_count = (PI * f_wheel_diameter) / i_cpr;
constexpr double odom_angular_coef = 1.0/f_wheel_separation;


SpringDuck::SpringDuck(ros::NodeHandle nh, ros::NodeHandle private_nh, double target_control_freq)
  : nh_(nh), private_nh_(private_nh), serial_(io_){
  private_nh_.param<double>("wheel_diameter", wheel_diameter_, 0.275); // 바퀴지름 meter
  private_nh_.param<double>("max_accel", max_accel_, 5.0);
  private_nh_.param<double>("max_speed", max_speed_, 1.0);
  private_nh_.param<double>("polling_timeout_", polling_timeout_, 10.0);
  private_nh_.param<double>("gear_ratio", gear_ratio_, 159);  // 1/53(모터) * 1/3(로봇기어)
  private_nh_.param<double>("wheel_base", wheel_base_, 0.51); // 바퀴 중심으로 부터 가로 길이 (robotbase meter)
  private_nh_.param<double>("encoder_pulse", encoder_pulse_, 1000*4); // 1000x4체배
  private_nh_.param<std::string>("port_name", port_, "/dev/ttyUSB0");
  private_nh_.param<int>("baudrate", baudrate_, 115200);

  connect();
  enable();
  boost::this_thread::sleep(boost::posix_time::milliseconds(100));
  reset_travel_offset();

  pose_pub = new realtime_tools::RealtimePublisher<nav_msgs::Odometry>(nh_, "odom", 10);
  float covariance[36] =	{0.05, 0, 0, 0, 0, 0,  // covariance on gps_x
				 0, 0.05, 0, 0, 0, 0,  // covariance on gps_y
				 0, 0, 0.05, 0, 0, 0,  // covariance on gps_z
				 0, 0, 0, 0.3, 0, 0,  // large covariance on rot x
				 0, 0, 0, 0, 0.3, 0,  // large covariance on rot y
				 0, 0, 0, 0, 0, 0.3};  // large covariance on rot z

	for(int i = 0; i < 36; i++) pose_pub->msg_.pose.covariance[i] = covariance[i];

  #if 1
  double diff_speed_left = -0.5;
  double diff_speed_right = 0.5;

  char buffer[128];
  int dl = diff_speed_left*4000;
  int dr = diff_speed_right*4000;
  sprintf(buffer, "SV%d,-%d;", dl, dr);
  ROS_INFO("SV: %s", buffer);
	write(buffer);

  reset_travel_offset();
#endif
}


bool SpringDuck::read_byte(uint8_t &res){
  try{
    serial_.read_some(boost::asio::buffer(&res, sizeof(res)));
    return true;
  }
  
  catch (const std::exception &exc){
    ROS_ERROR("error on read: %s; reopening", exc.what());
    connect();
    return false;
  }
}


size_t SpringDuck::write(std::string msg){
  boost::system::error_code ec;
  std::size_t l = serial_.write_some(boost::asio::buffer(msg), ec);
  boost::this_thread::sleep(boost::posix_time::milliseconds(15));
  if(ec) boost::asio::detail::throw_error(ec, "write_some");
  return l;
}


void SpringDuck::enable(){
  write("PE0001;");	// torque on
  write("SM0505;");  // velocity mode

}


void SpringDuck::disable(){
  write("SV0,0;");
  boost::this_thread::sleep(boost::posix_time::milliseconds(500));
  write("PD0001;");	// torque off
}


void SpringDuck::shutdown(){
  std::cout << "SpringDuck is done" << std::endl;
  disable();
}


void SpringDuck::connect(){
  try{
    serial_.close();
    serial_.open(port_);
    serial_.set_option(boost::asio::serial_port::baud_rate(baudrate_));
    ROS_INFO("SpringDuck is starting...");
  }
  catch (const std::exception &exc){
    ROS_ERROR("error on open(%s): %s; reopening after delay", port_.c_str(), exc.what());
    boost::this_thread::sleep(boost::posix_time::seconds(1));
  }
}


bool SpringDuck::get_encoder(int& left, int& right){
  bool ret = true;
  try{
    write("QP;");	// 60 page
    char buf[1024];
    boost::system::error_code ec;
    serial_.read_some(boost::asio::buffer(buf), ec);
    if(ec) boost::asio::detail::throw_error(ec, "read_some");
    std::vector<unsigned char> received_buffer;

    std::vector<std::string> tokens;
    boost::split(tokens, buf, boost::is_any_of(";"));

    for(size_t i = 0; i < tokens.size(); i++){
      if( strncmp( tokens[i].c_str(), "QP", 2) == 0 ){
        ret = get_encoder(tokens[i], left, right);
      }
    }
  }
  catch (const std::exception& exc){
    ROS_ERROR("error on read: %s; reopening", exc.what());
    connect();   
    ret = false; 
  }
  return ret;
}


bool SpringDuck::get_encoder(std::string buf, int& l, int& r){
  bool ret = true;
  try{
    l = 0; r = 0;
    std::vector<std::string> t1;
    boost::split(t1, buf, boost::is_any_of(","));
    if( t1.size() == 2 ){
      if( t1[0].length() == 8+2 && t1[1].length() == 8){
        std::string le = t1[0].substr(2, 8);
        l = (int)strtol(le.c_str(), NULL, 16);
        std::string re = t1[1];
        r = (int)strtol(re.c_str(), NULL, 16);
      }
    }
  }
  catch (const std::exception& exc){
    ROS_ERROR("exception caught %s", exc.what());
    ret = false;
  }
  return ret;
}


void SpringDuck::reset_travel_offset(){
  write("QEA55A;");	// 60 page
}


void SpringDuck::read(){
  static double pos_x = 0;
  static double pos_y = 0;
  static double theta = 0;
  static double last_time = 0;
  static double lin_velL_pre = 0;
  static double lin_velR_pre = 0;
  double diff_vel = 0;
  double alpha = 0;
  double dt = 0;
  tf::Quaternion q_new;
  static int pre_encoder_left = 0;
  static int pre_encoder_right = 0;  
  int encoder_left = 0;
  int encoder_right = 0;
  double odom_traction_factor = 0.610;
  
  ros::Time temp_time = ros::Time::now();
  double current_time = temp_time.toSec();  
  dt = current_time - last_time;

  if (last_time == 0) {
    last_time = current_time;
    ROS_INFO("read function is called first");
    return ;
  }

  if(get_encoder(encoder_left, encoder_right) ){    
    if(encoder_left == 0 || encoder_right == 0){
      ROS_ERROR("encoder can't to be zero left: %d, right: %d", encoder_left, encoder_right);
      return ;
    }
    encoder_right *= -1;
    ROS_DEBUG_STREAM("left encoder: " << encoder_left <<  ", right encoder: " << encoder_right);    
    double diff_left = encoder_left - pre_encoder_left;
    double diff_right = encoder_right - pre_encoder_right;
    pre_encoder_left = encoder_left;
    pre_encoder_right = encoder_right;
    ROS_DEBUG_STREAM("diff_left: " << diff_left << " diff_right: " << diff_right);
    double omega_left = (diff_left * distance_per_count) / dt;
    double omega_right = (diff_right * distance_per_count) / dt;
    ROS_DEBUG_STREAM("omega_left: " << omega_left << " omega_right: " << omega_right);

    double raw_left_vel = omega_left;
    double lin_vel_left = (1 - 0.55)*raw_left_vel + 0.55*lin_velL_pre;
    lin_velL_pre = lin_vel_left;

    double raw_right_vel = omega_right;
    double lin_vel_right = (1 - 0.55)*raw_right_vel + 0.55*lin_velR_pre;
    lin_velR_pre = lin_vel_right;

    double lin_vel = (lin_vel_left + lin_vel_right) / 2.0;
    double ang_vel = (lin_vel_left - lin_vel_right) / f_wheel_separation; // compare to alpha
    diff_vel = lin_vel_right - lin_vel_left;
    alpha = odom_angular_coef * diff_vel * odom_traction_factor;
    pos_x = pos_x + lin_vel * cos(theta) * dt;
    pos_y = pos_y + lin_vel * sin(theta) * dt;
    theta = (theta + alpha * dt);
    q_new = tf::createQuaternionFromRPY(0, 0, theta);
    quaternionTFToMsg(q_new, pose_pub->msg_.pose.pose.orientation);

    ROS_INFO_STREAM("linear velocity: " << lin_vel << " angular velocity: " << alpha);    
    ROS_DEBUG_STREAM("(current_time - last_time).toSec() " << dt);
    // If not moving, trust the encoders completely
    // otherwise set them to the ROS param
    if (lin_vel == 0 && alpha == 0) {
      pose_pub->msg_.twist.covariance[0] = 0.01 / 1e3;
      pose_pub->msg_.twist.covariance[7] = 0.01 / 1e3;
      pose_pub->msg_.twist.covariance[35] = 0.03 / 1e6;
    }
    else {
      pose_pub->msg_.twist.covariance[0] = 0.01;
      pose_pub->msg_.twist.covariance[7] = 0.01;
      pose_pub->msg_.twist.covariance[35] = 0.03;
    }

    pose_pub->msg_.pose.pose.position.x = pos_x;
    pose_pub->msg_.pose.pose.position.y = pos_y;

    pose_pub->msg_.twist.twist.linear.x = lin_vel;
    pose_pub->msg_.twist.twist.linear.y = 0.0;
    pose_pub->msg_.twist.twist.linear.z = 0.0;
    pose_pub->msg_.twist.twist.angular.x = 0.0;
    pose_pub->msg_.twist.twist.angular.y = 0.0;
    pose_pub->msg_.twist.twist.angular.z = alpha; // ang_vel;
    pose_pub->msg_.header.stamp = ros::Time::now();
    pose_pub->msg_.header.frame_id = "/odom";
    pose_pub->msg_.child_frame_id = "/base_link";

    if (pose_pub->trylock()){
      pose_pub->unlockAndPublish();
    }
  }
  last_time = current_time;
}


void SpringDuck::write() {
 
}

} // namespace spring

typedef boost::chrono::steady_clock time_source;

void controlLoop(spring::SpringDuck &duck,
                 controller_manager::ControllerManager &cm,
                 time_source::time_point &last_time) {
  
  time_source::time_point this_time = time_source::now();
  boost::chrono::duration<double> elapsed_duration = this_time - last_time;
  ros::Duration elapsed(elapsed_duration.count());
  last_time = this_time;
  duck.read();  
  cm.update(ros::Time::now(), elapsed);
  duck.write();
}
void sigintHandler(int sig) {    
    ros::shutdown();
}
void diagnosticLoop(spring::SpringDuck &duck){
}

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "springduck");
  ros::NodeHandle nh, private_nh("~");
  signal(SIGINT, sigintHandler);
  double control_frequency, diagnostic_frequency;
  private_nh.param<double>("control_frequency", control_frequency, 100.0);
  private_nh.param<double>("diagnostic_frequency", diagnostic_frequency, 1.0);
  
  spring::SpringDuck duck(nh, private_nh, control_frequency);
  controller_manager::ControllerManager cm(&duck, nh);

  ros::CallbackQueue my_queue;
  ros::AsyncSpinner my_spinner(1, &my_queue);

  time_source::time_point last_time = time_source::now();
  
  ros::TimerOptions control_timer(ros::Duration(1 / control_frequency), // Duration(frequency to second)
    boost::bind(controlLoop, boost::ref(duck), boost::ref(cm), boost::ref(last_time)), &my_queue);
  ros::Timer control_loop = nh.createTimer(control_timer);

  ros::TimerOptions diagnostic_timer(ros::Duration(1 / diagnostic_frequency), 
    boost::bind(diagnosticLoop, boost::ref(duck)), &my_queue);
  ros::Timer diagnostic_loop = nh.createTimer(diagnostic_timer);

  my_spinner.start();
  ros::spin();
  duck.shutdown();
  return 0;
}
