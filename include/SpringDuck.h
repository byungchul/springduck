
#pragma once

#include "diagnostic_updater/diagnostic_updater.h"
#include "hardware_interface/joint_state_interface.h"
#include "hardware_interface/joint_command_interface.h"
#include "hardware_interface/robot_hw.h"
#include "ros/ros.h"
#include "sensor_msgs/JointState.h"
#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/serial_port.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread/thread.hpp>
#include <string>

#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Vector3.h"
#include "geometry_msgs/Pose2D.h"
#include "geometry_msgs/PointStamped.h"


#include <realtime_tools/realtime_publisher.h>

#include <diagnostic_updater/diagnostic_updater.h>
#include <diagnostic_updater/publisher.h>


namespace
{
  const uint8_t LEFT = 0, RIGHT = 1;
};

namespace spring
{
class SpringDuck : public hardware_interface::RobotHW
{
public:
  SpringDuck(ros::NodeHandle nh, ros::NodeHandle private_nh, double target_control_freq);
  void shutdown();
  void read();
  void write();  
private:
  ros::NodeHandle nh_, private_nh_;
  hardware_interface::JointStateInterface joint_state_interface_;
  hardware_interface::VelocityJointInterface velocity_joint_interface_;
  ros::Publisher diagnostic_publisher_;
  double wheel_diameter_, max_accel_, max_speed_, gear_ratio_, wheel_base_, encoder_pulse_;
  double polling_timeout_;
  boost::asio::io_service io_;
  boost::asio::serial_port serial_;
  std::string port_;
  int baudrate_;

  struct Joint {
    double position;
    double position_offset;
    double velocity;
    double effort;
    double velocity_command;

    Joint() :
      position(0), velocity(0), effort(0), velocity_command(0)
    { }
  } joints_[4];
  
  void connect();
  size_t write(std::string msg);
  void enable();
  void disable();    
  bool read_byte(uint8_t &res);
  void reset_travel_offset();  
  bool get_encoder(int& left, int& right);
  bool get_encoder(std::string buf, int& l, int& r);


  realtime_tools::RealtimePublisher<nav_msgs::Odometry> * pose_pub;

};
}  // namespace

